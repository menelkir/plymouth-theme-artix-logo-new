plymouth-theme-artix-logo-new
=============================

A plymouth theme for Artix, based on plymouth-theme-arch-logo-new

===========
Themes used
===========

:plymouth-theme-arch-logo-new: https://aur.archlinux.org/packages/plymouth-theme-arch-logo-new/

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c
